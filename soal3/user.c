#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_MSG_SIZE 100

typedef struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
} msgbuf;

int main() {
    key_t key;
    int msgid;
    msgbuf message;

    // generate unique key
    if ((key = ftok("stream.c", 'B')) == -1) {
        perror("ftok");
        exit(1);
    }

    // create message queue
    if ((msgid = msgget(key, 0644)) == -1) {
        perror("msgget");
        exit(1);
    }

    // send message to stream receiver
    printf("Enter command: ");
    fgets(message.mtext, MAX_MSG_SIZE, stdin);
    message.mtype = 1;
    if (msgsnd(msgid, &message, strlen(message.mtext)+1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

    printf("Command sent: %s", message.mtext);

    return 0;
}
