#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_SONGS 100
#define MAX_LEN 100

typedef struct {
    long mtype;
    char mtext[MAX_LEN];
} message;

int main() {
    int msqid, status;
    key_t key;
    message msg;

    // Generate a unique key for the message queue
    key = ftok(".", 'a');
    if (key == -1) {
        perror("ftok failed");
        exit(EXIT_FAILURE);
    }

    // Create the message queue with read/write permissions for all users
    msqid = msgget(key, IPC_CREAT | 0666);
    if (msqid == -1) {
        perror("msgget failed");
        exit(EXIT_FAILURE);
    }

    printf("System started.\n");

    // Initialize song list
    char songs[MAX_SONGS][MAX_LEN] = {"Song 1", "Song 2", "Song 3"};
    int num_songs = 3;

    // Wait for incoming messages
    while (1) {
        // Receive a message
        status = msgrcv(msqid, &msg, MAX_LEN, 0, 0);
        if (status == -1) {
            perror("msgrcv failed");
            exit(EXIT_FAILURE);
        }

        // Parse command from the message
        char command[MAX_LEN];
        strcpy(command, msg.mtext);

        // Execute command
        if (strcmp(command, "list") == 0) {
            // List available songs
            printf("Available songs:\n");
            for (int i = 0; i < num_songs; i++) {
                printf("- %s\n", songs[i]);
            }
        } else if (strncmp(command, "play ", 5) == 0) {
            // Play a song
            char song_name[MAX_LEN];
            strcpy(song_name, command + 5); // remove "play " prefix
            int found = 0;
            for (int i = 0; i < num_songs; i++) {
                if (strcmp(song_name, songs[i]) == 0) {
                    printf("Playing %s\n", song_name);
                    found = 1;
                    break;
                }
            }
            if (!found) {
                printf("Error: song %s not found.\n", song_name);
            }
        } else if (strcmp(command, "quit") == 0) {
            // Quit the program
            printf("System stopped.\n");
            break;
        } else {
            printf("Error: invalid command.\n");
        }
    }

    // Remove the message queue
    if (msgctl(msqid, IPC_RMID, NULL) == -1) {
        perror("msgctl failed");
        exit(EXIT_FAILURE);
    }

    return 0;
}
