#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

    char *filename = argv[1];
    int fd = open(filename, O_RDONLY);
    if (fd == -1) {
        printf("Error: could not open file %s\n", filename);
        return 1;
    }

    int freq[256] = {0}; // array untuk menyimpan frekuensi kemunculan huruf
    char buffer[BUFFER_SIZE];
    ssize_t read_size;
    while ((read_size = read(fd, buffer, BUFFER_SIZE)) > 0) {
        for (int i = 0; i < read_size; i++) {
            freq[buffer[i]]++; // menambah frekuensi kemunculan huruf pada array freq
        }
    }

    int pipe_freq[2];
    if (pipe(pipe_freq) == -1) {
        printf("Error: could not create pipe\n");
        return 1;
    }

    pid_t pid = fork();
    if (pid == -1) {
        printf("Error: could not fork\n");
        return 1;
    } else if (pid == 0) {
        // child process
        close(pipe_freq[1]); // tutup sisi tulis pipe_freq pada child process

        // TODO: terima hasil perhitungan frekuensi kemunculan huruf dari parent process
        // lakukan kompresi file dengan menggunakan algoritma Huffman
        // simpan Huffman tree pada file terkompresi
        // ubah karakter pada file menjadi kode Huffman dan kirimkan ke program dekompresi menggunakan pipe
    } else {
        // parent process
        close(pipe_freq[0]); // tutup sisi baca pipe_freq pada parent process

        // kirim hasil perhitungan frekuensi kemunculan huruf ke child process
        write(pipe_freq[1], freq, sizeof(freq));

        wait(NULL); // tunggu child process selesai

        // TODO: baca Huffman tree dari file terkompresi
        // baca kode Huffman dan lakukan dekompresi
        // hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman
        // tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman
    }

    void compress_file(int fd_read, int fd_write) {
    // Membaca frekuensi huruf dari parent process
    freq_table freqs[MAX_CHAR];
    memset(freqs, 0, sizeof(freqs));
    read(fd_read, freqs, sizeof(freqs));

    // Membangun Huffman tree dari frekuensi huruf
    node_t* tree = build_huffman_tree(freqs);

    // Menyimpan Huffman tree ke dalam file terkompresi
    int bytes_written = write_tree(tree, fd_write);

    // Membuat tabel kode Huffman dari Huffman tree
    code_table code_table;
    build_code_table(tree, code_table);

    // Membaca file asli dan menulis kode Huffman ke dalam pipe
    char buffer[BUFFER_SIZE];
    int bytes_read;
    while ((bytes_read = read(fd_read, buffer, BUFFER_SIZE)) > 0) {
        for (int i = 0; i < bytes_read; i++) {
            code_t code = code_table[buffer[i]];
            write(fd_write, &code, sizeof(code));
        }
    }

    // Menghitung jumlah bit setelah kompresi
    int original_size = lseek(fd_read, 0, SEEK_END);
    int compressed_size = lseek(fd_write, 0, SEEK_CUR) - bytes_written;
    printf("Ukuran asli: %d bit, ukuran terkompresi: %d bit\n", original_size * 8, compressed_size * 8);

    // Menghapus Huffman tree dari memori
    destroy_tree(tree);
    }
    // Simpan Huffman tree pada file terkompresi
    fwrite(&tree, sizeof(tree), 1, compressed_file);

    // Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe
    rewind(file);
    unsigned char buffer[BUFFER_SIZE];
    size_t read_len;
    while ((read_len = fread(buffer, 1, BUFFER_SIZE, file)) > 0) {
    for (size_t i = 0; i < read_len; i++) {
        char *code = getCode(tree, buffer[i]);
        write(pipe_fd[1], code, strlen(code));
    	}
    }
    // Read Huffman tree from compressed file
    if (fread(&root, sizeof(Node), 1, compressed_file) != 1) {
    	perror("Error reading Huffman tree from compressed file");
    	exit(EXIT_FAILURE);
    }

    // Decode Huffman codes and write to output file
    rewind(compressed_file);
    fseek(compressed_file, sizeof(Node), SEEK_SET);

    Node *curr_node = &root;
    unsigned char bit_buffer;
    int bit_pos = 7;
    while (fread(&bit_buffer, sizeof(unsigned char), 1, compressed_file) == 1) {
    	while (bit_pos >= 0) {
        if (is_leaf(curr_node)) {
            fwrite(&curr_node->c, sizeof(unsigned char), 1, output_file);
            curr_node = &root;
        }
        if (bit_buffer & (1 << bit_pos)) {
            curr_node = curr_node->right;
        } else {
            curr_node = curr_node->left;
        }
        bit_pos--;
    }
    bit_pos = 7;
    }
    if (is_leaf(curr_node)) {
    	fwrite(&curr_node->c, sizeof(unsigned char), 1, output_file);
    }
    
    // check input argument
    if (argc != 2) {
        printf("Usage: %s <input_file>\n", argv[0]);
        return 1;
    }

    char *input_filename = argv[1];
    char *output_filename = "compressed.huff";

    // open input file for reading
    int input_fd = open(input_filename, O_RDONLY);
    if (input_fd == -1) {
        perror("open");
        return 1;
    }

    // create output file for writing
    int output_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (output_fd == -1) {
        perror("open");
        return 1;
    }

    // create pipes
    int fd1[2], fd2[2];
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        return 1;
    }

    // create child process
    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        return 1;
    }

    if (pid == 0) {
        // child process

        // close unused pipe ends
        close(fd1[1]); // read from parent
        close(fd2[0]); // write to parent

        // read frequency table from parent
        FrequencyTable freq_table;
        read(fd1[0], &freq_table, sizeof(FrequencyTable));

        // build Huffman tree from frequency table
        HuffmanTree tree;
        build_huffman_tree(&tree, &freq_table);

        // write Huffman tree to output file
        write_huffman_tree(&tree, output_fd);

        // encode input file using Huffman coding
        unsigned char buffer[BUFSIZE];
        int num_read, num_bits_written;
        while ((num_read = read(input_fd, buffer, BUFSIZE)) > 0) {
            for (int i = 0; i < num_read; i++) {
                encode_symbol(&tree, buffer[i], &num_bits_written);
                write(output_fd, &buffer[i], sizeof(unsigned char));
            }
        }

        // send encoded data to parent process
        close(input_fd); // finished reading input file
        close(output_fd); // finished writing compressed file
        close(fd1[0]); // finished reading frequency table
        write(fd2[1], &tree.num_bits, sizeof(int)); // send number of bits
        exit(0);
    } else {
        // parent process

        // close unused pipe ends
        close(fd1[0]); // write to child
        close(fd2[1]); // read from child

        // read input file and count frequency of each symbol
        FrequencyTable freq_table;
        init_frequency_table(&freq_table);
        unsigned char buffer[BUFSIZE];
        int num_read;
        while ((num_read = read(input_fd, buffer, BUFSIZE)) > 0) {
            for (int i = 0; i < num_read; i++) {
                count_symbol(&freq_table, buffer[i]);
            }
        }

    // send frequency table to child process
    close(input_fd);    

    close(fd);
    return 0;
}
