#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW_1 4 // jumlah baris pada matriks pertama
#define COL_1 2 // jumlah kolom pada matriks pertama
#define ROW_2 2 // jumlah baris pada matriks kedua
#define COL_2 5 // jumlah kolom pada matriks kedua
#define SHM_SIZE sizeof(int[ROW_1][COL_2]) // ukuran shared memory

// fungsi untuk menghitung faktorial sebuah bilangan
void* factorial(void* arg) {
    int num = *((int*) arg);
    long double result = 1;
    for (int i = 1; i <= num; i++) {
        result *= i;
    }
    long double* res_ptr = malloc(sizeof(long double));
    *res_ptr = result;
    pthread_exit(res_ptr);
}

int main() {
    int i, j, k;
    key_t key = 12345; // kunci untuk shared memory
    int shmid; // id dari shared memory
    void* status;

    // membuat shared memory segment untuk menyimpan hasil perkalian matriks
    shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // mengattach shared memory segment ke address space dari proses
    int (*shared_mat3)[COL_2] = shmat(shmid, NULL, 0);
    if (shared_mat3 == (void*) -1) {
        perror("shmat");
        exit(1);
    }

    // inisialisasi array of threads
    pthread_t threads[ROW_1][COL_2];

    struct timespec start_time, end_time; // deklarasi variabel untuk menyimpan waktu awal dan akhir

    clock_gettime(CLOCK_MONOTONIC, &start_time); // catat waktu awal

    // tampilkan matriks hasil perkalian yang diambil dari shared memory
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", shared_mat3[i][j]);
        }
        printf("\n");
    }

    // hitung faktorial setiap angka dari matriks menggunakan thread
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            int* num_ptr = &shared_mat3[i][j];
            if (pthread_create(&threads[i][j], NULL, factorial, (void*) num_ptr) != 0) {
                perror("pthread_create");
                exit(1);
            }
        }
    }

    // tampilkan hasil faktorial dari matriks ke layar dalam format seperti matriks
    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            if (pthread_join(threads[i][j], &status) != 0) {
                perror("pthread_join");
                exit(1);
            }
            long double* result_ptr = (long double*) status;
            long double result = *result_ptr;
            printf("%.0Lf ", result);
            free(result_ptr);
        }
        printf("\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &end_time); // catat waktu akhir

    // detach shared memory segment dari address space proses
    if (shmdt(shared_mat3) == -1) {
        perror("shmdt");
        exit(1);
    }

    // // hapus shared memory segment
    // if (shmctl(shmid, IPC_RMID, NULL) == -1) {
    //     perror("shmctl");
    //     exit(1);
    // }

    long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000000000 + end_time.tv_nsec - start_time.tv_nsec; // hitung waktu proses dalam satuan nanosecond

printf("\nWaktu proses: %ld nanosecond\n", elapsed_time); // tampilkan waktu proses ke layar

return 0;
}

