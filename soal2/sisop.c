#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW_1 4 // jumlah baris pada matriks pertama
#define COL_1 2 // jumlah kolom pada matriks pertama
#define ROW_2 2 // jumlah baris pada matriks kedua
#define COL_2 5 // jumlah kolom pada matriks kedua
#define SHM_SIZE sizeof(int[ROW_1][COL_2]) // ukuran shared memory

// fungsi untuk menghitung faktorial sebuah bilangan
long double factorial(int num) {
    long double result = 1;
    for (int i = 1; i <= num; i++) {
        result *= i;
    }
    return result;
}

int main() {
    int i, j, k;
    key_t key = 12345; // kunci untuk shared memory
    int shmid; // id dari shared memory
    struct timespec start_time, end_time;
    long elapsed_time;

    // membuat shared memory segment untuk menyimpan hasil perkalian matriks
    shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // mengattach shared memory segment ke address space dari proses
    int (*shared_mat3)[COL_2] = shmat(shmid, NULL, 0);
    if (shared_mat3 == (void*) -1) {
        perror("shmat");
        exit(1);
    }

    clock_gettime(CLOCK_MONOTONIC, &start_time); // catat waktu awal

    // tampilkan matriks hasil perkalian yang diambil dari shared memory
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", shared_mat3[i][j]);
        }
        printf("\n");
    }

    // hitung faktorial setiap angka dari matriks tanpa menggunakan thread
    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            long double result = factorial(shared_mat3[i][j]);
            printf("%.0Lf ", result);
        }
        printf("\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &end_time); // catat waktu akhir

    elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000000000 + end_time.tv_nsec - start_time.tv_nsec; // hitung waktu proses dalam satuan nanosecond

    printf("\nWaktu proses: %ld nanosecond\n", elapsed_time); // tampilkan waktu proses ke layar

    // detach shared memory segment dari address space proses
    if (shmdt(shared_mat3) == -1) {
        perror("shmdt");
        exit(1);
    }

    // // hapus shared memory segment
    // if (shmctl(shmid, IPC_RMID, NULL) == -1) {
    //     perror("shmctl");
    //     exit(1);
    // }

    return 0;
}
