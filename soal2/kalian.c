#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW_1 4 // jumlah baris pada matriks pertama
#define COL_1 2 // jumlah kolom pada matriks pertama
#define ROW_2 2 // jumlah baris pada matriks kedua
#define COL_2 5 // jumlah kolom pada matriks kedua
#define SHM_SIZE sizeof(int[ROW_1][COL_2]) // ukuran shared memory

int main() {
    int i, j, k;
    int mat1[ROW_1][COL_1], mat2[ROW_2][COL_2], mat3[ROW_1][COL_2]; // matriks pertama, kedua, dan hasil
    key_t key = 12345; // kunci untuk shared memory
    int shmid; // id dari shared memory

    srand(time(NULL)); // inisialisasi random seed

    // membuat shared memory segment dengan ukuran SHM_SIZE dan hak akses 0666
    shmid = shmget(key, SHM_SIZE, 0666|IPC_CREAT);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    // mengattach shared memory segment ke address space dari proses
    int (*shared_mat3)[COL_2] = shmat(shmid, NULL, 0);
    if (shared_mat3 == (void*) -1) {
        perror("shmat");
        exit(1);
    }

    // isi matriks pertama dengan angka random dari 1-5
    printf("Matriks pertama:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_1; j++) {
            mat1[i][j] = rand() % 5 + 1;
            printf("%d ", mat1[i][j]);
        }
        printf("\n");
    }

    // isi matriks kedua dengan angka random dari 1-4
    printf("Matriks kedua:\n");
    for (i = 0; i < ROW_2; i++) {
        for (j = 0; j < COL_2; j++) {
            mat2[i][j] = rand() % 4 + 1;
            printf("%d ", mat2[i][j]);
        }
        printf("\n");
    }

    // perkalian matriks pertama dan kedua
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            shared_mat3[i][j] = 0;
            for (k = 0; k < ROW_2; k++) {
                shared_mat3[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    // tampilkan matriks hasil perkalian
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", shared_mat3[i][j]);
        }
        printf("\n");
    }

    // detach shared memory segment dari address space proses
    if (shmdt(shared_mat3) == -1) {
        perror("shmdt");
        exit(1);
    }

    return 0;
}
