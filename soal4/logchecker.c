#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_EXT_COUNT 100
#define MAX_EXT_LEN 10

char ext_list[MAX_EXT_COUNT][MAX_EXT_LEN];

void printAccessCount() {
  FILE * log_file = fopen("log.txt", "r");
  char line[256];
  int access_count = 0;

  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
      if (strstr(line, "ACCESSED") != NULL) {
        access_count++;
      }
    }
    fclose(log_file);

    printf("Banyaknya ACCESSED: %d\n", access_count);
  } else {
    printf("Gagal membuka file log.txt\n");
  }
}

void printFolderList() {
  FILE * log_file = fopen("log.txt", "r");
  char line[256];
  char folder_name[256];
  int folder_counts[MAX_EXT_COUNT] = {
    0
  };

  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
      if (strstr(line, "MADE") != NULL) {
        char * token = strtok(line, " ");
        while (token != NULL) {
          if (strstr(token, "categorized/") != NULL) {
            strcpy(folder_name, token);
            break;
          }
          token = strtok(NULL, " ");
        }

        for (int i = 0; i < MAX_EXT_COUNT; i++) {
          if (strcmp(ext_list[i], "") == 0) {
            break;
          }

          if (strstr(folder_name, ext_list[i]) != NULL) {
            folder_counts[i]++;
            break;
          }
        }
      }
    }
    fclose(log_file);

    printf("List folder:\n");
    for (int i = 0; i < MAX_EXT_COUNT; i++) {
      if (strcmp(ext_list[i], "") == 0) {
        break;
      }

      printf("%s : %d\n", ext_list[i], folder_counts[i]);
    }
  } else {
    printf("Gagal membuka file log.txt\n");
  }
}

void printFileCount() {
  FILE * log_file = fopen("log.txt", "r");
  char line[256];
  int png_count = 0;
  int jpg_count = 0;
  int emc_count = 0;
  int xyz_count = 0;
  int js_count = 0;
  int py_count = 0;
  int txt_count = 0;
  int other_count = 0;

  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
      if (strstr(line, "ACCESSED") != NULL) {
        continue;
      }

      if (strstr(line, "MOVED") != NULL) {
        char * token = strtok(line, " ");
        char file_ext[MAX_EXT_LEN];

        while (token != NULL) {
          if (strstr(token, ".") != NULL) {
            strcpy(file_ext, token);
            break;
          }
          token = strtok(NULL, " ");
        }

        if (strstr(file_ext, "png") != NULL) {
          png_count++;
        } else if (strstr(file_ext, "jpg") != NULL) {
          jpg_count++;
        } else if (strstr(file_ext, "emc") != NULL) {
          emc_count++;
        } else if (strstr(file_ext, "xyz") != NULL) {
          xyz_count++;
        } else if (strstr(file_ext, "js") != NULL) {
          js_count++;
        } else if (strstr(file_ext, "py") != NULL) {
          py_count++;
        } else if (strstr(file_ext, "txt") != NULL) {
          txt_count++;
        } else {
          other_count++;
        }
      }
    }
    fclose(log_file);

    printf("Jumlah file txt: %d\n", txt_count);
    printf("Jumlah file emc: %d\n", emc_count);
    printf("Jumlah file jpg: %d\n", jpg_count);
    printf("Jumlah file png: %d\n", png_count);
    printf("Jumlah file js: %d\n", js_count);
    printf("Jumlah file xyz: %d\n", xyz_count);
    printf("Jumlah file py: %d\n", py_count);
    printf("Jumlah file other: %d\n", other_count);
  } else {
    printf("Gagal membuka file log.txt\n");
  }
}

int main() {
  char line[256];
  int ext_count = 0;

  FILE * ext_file = fopen("extensions.txt", "r");
  if (ext_file != NULL) {
    while (fgets(line, sizeof(line), ext_file)) {
      line[strcspn(line, "\r\n")] = 0;
      strcpy(ext_list[ext_count], line);
      ext_count++;
    }
    fclose(ext_file);
  } else {
    printf("Gagal membuka file extensions.txt\n");
    return 1;
  }

  printf("Pilih operasi:\n");
  printf("1. Banyaknya akses file\n");
  printf("2. List folder berdasarkan ekstensi file\n");
  printf("3. Jumlah file berdasarkan jenis\n");

  int choice;
  scanf("%d", & choice);

  switch (choice) {
  case 1:
    printAccessCount();
    break;
  case 2:
    printFolderList();
    break;
  case 3:
    printFileCount();
    break;
  default:
    printf("Operasi tidak valid\n");
    break;
  }

  return 0;
}