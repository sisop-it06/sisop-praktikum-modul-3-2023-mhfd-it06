# sisop-praktikum-modul-3-2023-MHFD-IT06


- Fathika Afrine Azaruddin - 5027211016
- Athallah Narda Wiyoga - 5027211041
- Gilbert Immanuel Hasiholan - 5027211056

# Soal no. 1

# Soal no. 2

### Penjelasan Cinta.c

#### Header dan Definisi Konstanta

Pada bagian ini, beberapa header-file di-include yang diperlukan untuk menggunakan fungsi-fungsi standar, thread, shared memory, dan waktu.Selanjutnya, terdapat definisi konstanta `ROW_1`, `COL_1`, `ROW_2`, dan `COL_2` yang merepresentasikan ukuran matriks pertama dan kedua.
Konstanta `SHM_SIZE` digunakan untuk menentukan ukuran shared memory dalam byte.

#### Fungsi factorial

Fungsi factorial adalah sebuah fungsi yang digunakan untuk menghitung faktorial dari sebuah bilangan. Fungsi ini menerima argumen dalam bentuk pointer dan mengembalikan pointer. Pertama, nilai dari pointer argumen di-casting ke tipe data int dan disimpan dalam variabel num. Variabel result diinisialisasi dengan 1. Selanjutnya, dilakukan iterasi dari 1 hingga num dengan mengalikan setiap angka dengan result. Kemudian, dilakukan alokasi memori dinamis untuk menyimpan hasil faktorial dalam tipe data long double. Hasil faktorial disimpan dalam variabel `res_ptr`.
Terakhir, fungsi ini mengakhiri eksekusi thread dengan memanggil `pthread_exit` dan mengembalikan pointer `res_ptr`.

#### Fungsi main

Fungsi main merupakan titik masuk utama program. Pertama, beberapa variabel lokal dan variabel key di-deklarasikan. Variabel `shmid` digunakan untuk menyimpan ID dari shared memory segment. Variabel status digunakan untuk menyimpan status return dari fungsi `pthread_join`. Shared memory segment dibuat menggunakan shmget dengan menggunakan key dan `SHM_SIZE`. Kemudian, shared memory segment di-attach ke address space proses menggunakan shmat.
Variabel `shared_mat3` dideklarasikan sebagai pointer ke array dua dimensi yang akan mewakili matriks hasil perkalian yang disimpan dalam shared memory. Array of threads threads `[ROW_1]` `[COL_2]` diinisialisasi untuk menyimpan ID thread. Variabel `start_time` dan `end_time` dideklarasikan sebagai struct timespec untuk menyimpan waktu awal dan akhir. Waktu awal dicatat menggunakan `clock_gettime` dengan menggunakan `CLOCK_MONOTONIC`.
Matriks hasil perkalian yang diambil dari shared memory ditampilkan ke layar menggunakan perulangan for. Selanjutnya, faktorial dari setiap elemen matriks dihitung menggunakan thread.
Setiap thread diinisialisasi menggunakan pthread_create dengan argumen berupa pointer ke elemen matriks. Hasil faktorial dari matriks ditampilkan ke layar dalam format matriks menggunakan perulangan for. Hasil faktorial yang disimpan dalam pointer status di-deallocate menggunakan free.

### Penjelasan Kalian.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW_1 4 // jumlah baris pada matriks pertama
#define COL_1 2 // jumlah kolom pada matriks pertama
#define ROW_2 2 // jumlah baris pada matriks kedua
#define COL_2 5 // jumlah kolom pada matriks kedua
#define SHM_SIZE sizeof(int[ROW_1][COL_2]) // ukuran shared memory
```

Program diawali dengan header file yang akan digunakan dan mendefinisikan beberapa konstanta seperti ROW_1, COL_1, ROW_2, COL_2, dan SHM_SIZE. Konstanta tersebut menunjukkan ukuran dari matriks pertama, matriks kedua, dan ukuran shared memory yang akan digunakan untuk menyimpan hasil perkalian matriks.

```c
int main() {
    int i, j, k;
    int mat1[ROW_1][COL_1], mat2[ROW_2][COL_2], mat3[ROW_1][COL_2]; // matriks pertama, kedua, dan hasil
    key_t key = 12345; // kunci untuk shared memory
    int shmid; // id dari shared memory

    srand(time(NULL)); // inisialisasi random seed
```

Fungsi main() kemudian dideklarasikan dengan variabel-variabel i, j, k, mat1, mat2, mat3, key, dan shmid.
mat1, mat2, dan mat3 adalah matriks pertama, kedua, dan hasil yang akan digunakan di program.
key adalah kunci yang akan digunakan untuk mengakses shared memory.
shmid adalah id dari shared memory yang akan digunakan di program.
Kemudian, seed untuk fungsi rand() diinisialisasi dengan waktu saat ini menggunakan fungsi srand().

```c
    // membuat shared memory segment dengan ukuran SHM_SIZE dan hak akses 0666
    shmid = shmget(key, SHM_SIZE, 0666|IPC_CREAT);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }
```

Baris ini akan membuat shared memory segment menggunakan fungsi shmget().
shmget() menerima tiga argumen yaitu key yang digunakan untuk mengidentifikasi shared memory segment, SHM_SIZE yang merupakan ukuran shared memory, dan hak akses yang diberikan (dalam hal ini adalah 0666|IPC_CREAT).
Jika terjadi kesalahan pada fungsi shmget(), program akan mencetak pesan kesalahan menggunakan fungsi perror() dan kemudian keluar dari program menggunakan fungsi exit().

```c
    // mengattach shared memory segment ke address space dari proses
    int (*shared_mat3)[COL_2] = shmat(shmid, NULL, 0);
    if (shared_mat3 == (void*) -1) {
        perror("shmat");
        exit(1);
    }
```
Baris ini akan mengattach shared memory segment ke address space dari proses.

```c
// isi matriks pertama dengan angka random dari 1-5
    printf("Matriks pertama:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_1; j++) {
            mat1[i][j] = rand() % 5 + 1;
            printf("%d ", mat1[i][j]);
        }
        printf("\n");
    }
```

Bagian ini mengisi matriks mat1 dengan bilangan acak antara 1 dan 5 menggunakan fungsi rand(). Kemudian mencetak nilai mat1 ke konsol.

```c
    // isi matriks kedua dengan angka random dari 1-4
    printf("Matriks kedua:\n");
    for (i = 0; i < ROW_2; i++) {
        for (j = 0; j < COL_2; j++) {
            mat2[i][j] = rand() % 4 + 1;
            printf("%d ", mat2[i][j]);
        }
        printf("\n");
    }
```

Mirip dengan bagian sebelumnya, kode ini mengisi matriks mat2 dengan bilangan acak antara 1 dan 4 menggunakan fungsi rand(). Kemudian mencetak nilai mat2 ke konsol.

```c
    // perkalian matriks pertama dan kedua
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            shared_mat3[i][j] = 0;
            for (k = 0; k < ROW_2; k++) {
                shared_mat3[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }
```

Bagian ini melakukan perkalian matriks antara mat1 dan mat2 dan menyimpan hasilnya di shared_mat3.
Ini menggunakan loop bersarang untuk mengulang baris mat1, kolom mat2, dan dimensi umum mat1 dan mat2.
Variabel shared_mat3[i][j] bertambah dengan produk elemen yang sesuai dari mat1 dan mat2.

```c
    // tampilkan matriks hasil perkalian
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW_1; i++) {
        for (j = 0; j < COL_2; j++) {
            printf("%d ", shared_mat3[i][j]);
        }
        printf("\n");
    }
```

Bagian ini mencetak hasil matriks shared_mat3 ke konsol.

```c
    // detach shared memory segment dari address space proses
    if (shmdt(shared_mat3) == -1) {
        perror("shmdt");
        exit(1);
    }
```

Kode ini melepaskan segmen memori bersama dari ruang alamat proses menggunakan fungsi shmdt().
Jika terjadi kesalahan selama detasemen, pesan kesalahan dicetak menggunakan perror(), dan program keluar.

```c
    return 0;
}
```

Terakhir, fungsi main() mengembalikan 0 untuk menunjukkan eksekusi program yang sukses.
Singkatnya, program menghasilkan dua matriks (mat1 dan mat2) dengan angka acak, melakukan perkalian matriks padanya, menyimpan hasilnya di segmen memori bersama.

### Penjelasan Sisop.c

- Direktif #include:

Mendefinisikan beberapa library yang diperlukan untuk program ini, seperti stdio.h, stdlib.h, sys/ipc.h, sys/shm.h, dan time.h.

- Macro Definition:

ROW_1, COL_1, ROW_2, dan COL_2 adalah konstanta yang menyimpan ukuran matriks.
SHM_SIZE adalah ukuran shared memory yang dihitung menggunakan sizeof untuk menyimpan matriks hasil perkalian.

- Fungsi factorial:

Fungsi ini menghitung faktorial dari sebuah bilangan.
Menggunakan loop for untuk mengalikan angka dari 1 hingga bilangan tersebut.

- Fungsi main:

Variabel key digunakan sebagai kunci untuk shared memory.
Variabel shmid digunakan untuk menyimpan id dari shared memory.
Variabel start_time, end_time, dan elapsed_time digunakan untuk mengukur waktu proses.

- Membuat Shared Memory:

Menggunakan fungsi shmget untuk membuat shared memory dengan ukuran SHM_SIZE menggunakan key.
Jika shmget mengembalikan nilai -1, maka terjadi error.

- Mengattach Shared Memory:

Menggunakan fungsi shmat untuk mengattach shared memory ke address space proses.
Hasil attach shared memory disimpan dalam variabel shared_mat3.
Jika shmat mengembalikan (void*) -1, maka terjadi error.

- Menampilkan Matriks Hasil Perkalian:

Menggunakan loop for untuk mengakses dan menampilkan elemen matriks shared_mat3.

- Menghitung Faktorial Matriks:

Menggunakan loop for untuk mengakses setiap elemen matriks shared_mat3.
Memanggil fungsi factorial untuk menghitung faktorial dari setiap elemen matriks.
Menampilkan hasil faktorial ke layar.

- Mengukur Waktu Proses:

Menggunakan fungsi clock_gettime untuk mendapatkan waktu awal dan waktu akhir proses.
Menghitung waktu proses dalam satuan nanosecond dengan menghitung selisih waktu awal dan waktu akhir.

- Detach Shared Memory:

Menggunakan fungsi shmdt untuk melepas shared memory dari address space proses.
Jika shmdt mengembalikan nilai -1, maka terjadi error.

- Menghapus Shared Memory (opsional):

Jika perlu, Anda dapat menggunakan fungsi shmctl dengan parameter IPC_RMID untuk menghapus shared memory.
Baris kode ini di-comment karena belum digunakan. Jika Anda ingin menghapus shared memory, Anda dapat menghapus tanda komentar dan melibatkan baris kode ini.

- Mengembalikan Nilai:

Fungsi main mengembalikan nilai 0 untuk menandakan bahwa program selesai berjalan dengan sukses.
Program ini mencetak matriks hasil perkalian dan faktorialnya

![check](https://cdn.discordapp.com/attachments/1067327620938747946/1106866912392134736/image.png)

# Soal no. 3

# Soal no. 4

unzip.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>
```
Baris ini mengimport beberapa library yang akan digunakan dalam program. 

```c
void downloadHehe() {
    char* args[] = {"curl", "-L", "-o", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error downloading zip\n");
            exit(EXIT_FAILURE);
        }
    }
}
```
Fungsi downloadHehe() adalah fungsi untuk mendownload file zip dari internet menggunakan command curl.
Pertama-tama kita deklarasikan array of pointer char bernama args yang berisi daftar argumen yang akan dijalankan oleh command curl. Di sini kita akan menjalankan command curl -L -o hehe.zip https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download. Argumen "-L" digunakan agar curl dapat mengikuti redirect dari link yang diberikan, sedangkan argumen "-o" digunakan untuk menentukan nama file output dari hasil download.
Kemudian, kita melakukan fork() untuk membuat sebuah child process yang akan menjalankan command curl tersebut. Jika fork gagal, maka program akan keluar dan menampilkan pesan error. Jika berhasil membuat child process, maka parent process akan menunggu hingga child process selesai dieksekusi dengan menggunakan fungsi waitpid().
Apabila status dari proses yang telah dijalankan oleh child process adalah normal dan exit status yang dikembalikan oleh proses tersebut bukan 0, maka akan ditampilkan pesan error "Error downloading zip".

```c
void extractHehe() {
    char* args[] = {"unzip", "hehe.zip", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error extracting zip\n");
            exit(EXIT_FAILURE);
        }
    }
}
```
Fungsi extractHehe() adalah fungsi untuk mengekstrak isi dari file zip yang telah didownload sebelumnya menggunakan command unzip.
Pertama-tama kita deklarasikan array of pointer char yang berisi daftar argumen yang akan dijalankan oleh command unzip. Di sini kita akan menjalankan command unzip hehe.zip.
Kemudian, kita melakukan fork() untuk membuat sebuah child process yang akan menjalankan command unzip tersebut. Jika fork gagal, maka program akan keluar dan menampilkan pesan error. Jika berhasil membuat child process, maka parent process akan menunggu hingga child process selesai dieksekusi dengan menggunakan fungsi waitpid().
Apabila status dari proses yang telah dijalankan oleh child process adalah normal dan exit status yang dikembalikan oleh proses tersebut bukan 0, maka akan ditampilkan pesan error "Error extracting zip".

```c
int main() {
    downloadHehe();
    extractHehe();

    return 0;
}
```
Bagian dari C script tersebut adalah fungsi utama (main program) yang akan dijalankan pertama kali ketika program dijalankan.
Fungsi downloadHehe() dan extractHehe() dipanggil secara berurutan pada fungsi utama dengan tujuan untuk melakukan download file zip dari internet menggunakan command curl, kemudian mengekstrak isi dari file zip tersebut menggunakan command unzip.
Setelah kedua fungsi tersebut dilakukan, program akan mengembalikan nilai 0 ke sistem operasi melalui return statement sebagai tanda bahwa program telah selesai dijalankan dengan sukses.

Hasil Output:
https://ibb.co/zn6wJvd

categorize.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
#include <pthread.h>
#include <time.h>
```
Baris-baris di atas adalah direktif preprosesor yang mengimpor berbagai library standar yang akan digunakan dalam program.

```c
#define MAX_EXT_COUNT 100
#define MAX_EXT_LEN 10
```
Bagian ini mendefinisikan dua konstanta dengan nama MAX_EXT_COUNT dan MAX_EXT_LEN. MAX_EXT_COUNT menentukan jumlah maksimum ekstensi file yang akan diproses, dan MAX_EXT_LEN menentukan panjang maksimum dari setiap ekstensi.

```c
char *extensions[] = {"jpg", "txt", "js", "py", "png", "emc", "xyz"};
char *files_dir = "/home/kali/Documents/Sisop/shift-3/soal4/files";
char *dir_name;
```
Dalam bagian ini, terdapat beberapa variabel dengan tipe data char *. Variabel extensions adalah array yang berisi daftar ekstensi file yang akan diproses. Variabel files_dir berisi path direktori tempat file-file akan ditempatkan. Variabel dir_name akan digunakan nanti dalam fungsi categorizeFiles.

```c
int ext_size = sizeof(extensions) / sizeof(extensions[0]);
const int maximum = 10;
```
Variabel ext_size menyimpan jumlah elemen dalam array extensions. Variabel maximum menyimpan angka 10 yang digunakan dalam pembuatan direktori yang dihasilkan.

```c
time_t current_time;
struct tm *timeline;
```
Variabel current_time bertipe time_t digunakan untuk menyimpan waktu saat ini, dan timeline adalah pointer ke struktur tm yang digunakan untuk menyimpan informasi waktu.
```c
char buff[80], valid_dir[256];
char src[256], dest[256];
char subdir[256], destination[256], directory_path[256], dest_folder[256];
```
Baris di atas mendefinisikan beberapa array karakter yang akan digunakan dalam program. Array tersebut digunakan untuk menyimpan path direktori, nama file, dan pesan log.
```c
char ext_list[MAX_EXT_COUNT][MAX_EXT_LEN];
```
Array ext_list merupakan array dua dimensi yang digunakan untuk menyimpan daftar ekstensi file yang akan diproses.

```c
void *categorizeFiles(void *dir_name)
```
Fungsi categorizeFiles digunakan untuk mengkategorikan file dalam sebuah direktori. Fungsi ini menerima argumen berupa nama direktori yang akan dikategorikan.

```c
    DIR *dir;
    struct dirent *ent;
    char current_dir[256];
    strcpy(current_dir, dir_name);
    char logs[512];

    snprintf(logs, sizeof(logs), "ACCESSED %s", current_dir);
    createLog(logs);
```
Dalam fungsi categorizeFiles, variabel dir dan ent digunakan untuk membaca isi direktori. Variabel current_dir digunakan untuk menyimpan nama direktori yang sedang diakses. Kemudian, fungsi strcpy dipanggil untuk menyalin nama direktori dari argumen dir_name ke variabel current_dir. Variabel logs digunakan untuk menyimpan pesan aksi yang dilakukan. Pesan tersebut dapat diubah sesuai dengan aksi yang dilakukan pada file atau direktori. Pada baris ketiga di atas, snprintf digunakan untuk membuat pesan log bahwa direktori telah diakses. Pesan tersebut disimpan di variabel logs dan kemudian dipass ke dalam fungsi createLog.

```c
    if ((dir = opendir(current_dir)) != NULL)
    {
        while ((ent = readdir(dir)) != NULL)
        {
```
Variabel dir digunakan untuk membuka direktori yang sedang diakses. Jika direktori berhasil dibuka, maka program akan membaca seluruh isi direktori menggunakan loop while pada baris sebelumnya.

```c
            if (ent->d_type == DT_REG)
```
Pada loop while pertama dalam fungsi categorizeFiles, dilakukan pengecekan apakah file yang ditemukan merupakan file regular atau bukan. Jika iya, maka program akan melakukan pengecekan ekstensi file tersebut.

```c
                char *validExtension = strrchr(ent->d_name, '.');
                int validFiles = 0;
                char valid_ext[256] = "other";

                if (validExtension)
                {
                    validExtension++;
                    strcpy(valid_ext, validExtension);

                    for (char *p = valid_ext; *p; ++p)
                    {
                        *p = tolower(*p);
                    }

                    for (int i = 0; i < ext_size; i++)
                    {
                        if (strcmp(valid_ext, extensions[i]) == 0)
                        {
                            validFiles = 1;
                            break;
                        }
                    }
                }
```
Jika file adalah file regular, program akan mencari ekstensi file tersebut dengan menggunakan fungsi strrchr. Fungsi ini digunakan untuk mencari karakter titik terakhir pada nama file. Kemudian, variabel valid_ext akan diisi dengan ekstensi file yang didapat dari proses pencarian tadi. Ekstensi file akan diubah menjadi huruf kecil semua menggunakan loop for, kemudian akan dilakukan pengecekan pada array extensions apakah ekstensi tersebut termasuk ekstensi yang valid.

```c
                if (!validFiles)
                {
                    snprintf(dest_folder, sizeof(dest_folder), "categorized/other");

                    if (dest_folder == NULL)
                    {
                        mkdir(dest_folder, 0700);
                    }

                    snprintf(src, sizeof(src), "%s/%s", current_dir, ent->d_name);
                    snprintf(dest, sizeof(dest), "%s/%s", dest_folder, ent->d_name);

                    rename(src, dest);
                    snprintf(logs, sizeof(logs), "MOVED %s file : %s > %s", valid_ext, src, dest);
                    createLog(logs);
                }
```
Jika ekstensi file tidak termasuk dalam daftar ekstensi yang valid, maka file akan dipindahkan ke direktori categorized/other. Kemudian, variabel src dan dest digunakan untuk menyimpan path asal dan tujuan file setelah dipindahkan. Fungsi rename dipanggil untuk memindahkan file dari src ke dest. Pesan log dibuat menggunakan snprintf kemudian disimpan dengan fungsi createLog.

```c
                else
                {
                    snprintf(destination, sizeof(destination), "categorized/%s", valid_ext);

                    struct stat st;
                    if (stat(destination, &st) == -1)
                    {
                        mkdir(destination, 0700);
                        snprintf(logs, sizeof(logs), "MADE %s", destination);
                        createLog(logs);
                    }
```
Jika ekstensi file termasuk dalam daftar ekstensi yang valid, maka program akan membuat direktori dengan nama categorized/valid_ext jika belum ada. Fungsi mkdir digunakan untuk membuat direktori tersebut. Pesan log juga dibuat menggunakan snprintf kemudian disimpan dengan fungsi createLog.

```c
                    int dirs = 1;
                    strcpy(dest_folder, destination);
```
Pada baris ini variabel dirs diinisialisasi dengan 1 dan dest_folder diisi dengan nilai dari variabel destination menggunakan fungsi strcpy.

```c
                    while (1)
                    {
                        DIR *dest_dir = opendir(dest_folder);
                        int count = 0;

                        if (dest_dir)
                        {
                            struct dirent *file_ent;
                            while ((file_ent = readdir(dest_dir)) != NULL)
                            {
                                if (file_ent->d_type == DT_REG)
                                {
                                    count++;
                                }
                            }
                            closedir(dest_dir);
                        }

                        if (count < maximum)
                        {
                            break;
                        }
```
Pada baris ini akan dilakukan looping selama kondisi bernilai true atau tidak sama dengan 0 atau false. Variabel dest_dir berisi pointer ke direktori yang ditunjuk oleh dest_folder. Kemudian, variabel count diinisialisasi dengan 0. Jika dest_dir berhasil dibuka, maka akan dilakukan loop sebanyak file yang ada pada direktori tersebut. Dalam setiap iterasi loop, variabel file_ent diisi dengan informasi tentang file yang sedang ditunjuk oleh loop. Jika jenis file tersebut adalah file reguler (regular file), maka variabel count akan ditambah 1. Setelah semua file dalam direktori telah dilalui, direktori akan ditutup. Jika jumlah file dalam direktori kurang dari maximum, maka loop akan dihentikan dengan break.

```c
                       dirs++;
                        snprintf(dest_folder, sizeof(dest_folder), "%s (%d)", destination, dirs);
                        if (stat(dest_folder, &st) == -1)
                        {
                            mkdir(dest_folder, 0700);
                            snprintf(logs, sizeof(logs), "MADE %s", dest_folder);
                            createLog(logs);
                        }
                    }
```
Jika jumlah file dalam direktori tersebut lebih dari atau sama dengan maximum, maka variabel dirs akan ditambah 1. Kemudian, variabel dest_folder akan diisi dengan format string yang berisi nama folder dan dirs. Jika fungsi stat pada dest_folder mengembalikan nilai -1, artinya folder belum ada, maka akan dibuat folder baru dengan nama dest_folder menggunakan fungsi mkdir dengan permission 0700. Kemudian log pesan "MADE <nama folder>" akan dibuat menggunakan fungsi snprintf dan disimpan pada variabel logs. Fungsi createLog kemudian dipanggil untuk mencatat pesan tersebut ke dalam file log. Looping akan terus berjalan hingga ditemukan folder kosong dengan jumlah file kurang dari maximum.

```c
                    snprintf(src, sizeof(src), "%s/%s", current_dir, ent->d_name);
                    snprintf(dest, sizeof(dest), "%s/%s", dest_folder, ent->d_name);

                    rename(src, dest);
                    snprintf(logs, sizeof(logs), "MOVED %s file : %s > %s", valid_ext, src, dest);
                    createLog(logs);
```
Bagian tersebut adalah sebuah loop untuk memindahkan file-file dengan ekstensi tertentu dari suatu direktori ke direktori lainnya.

```c
            else if (ent->d_type == DT_DIR && ent->d_name[0] != '.')
            {
                
                snprintf(subdir, sizeof(subdir), "%s/%s", current_dir, ent->d_name);

                pthread_t thread;
                pthread_create(&thread, NULL, categorizeFiles, (void *)subdir);
                pthread_join(thread, NULL);
            }
        }
        closedir(dir);
    }

    return NULL;
}
```
Jika file yang ditemukan adalah direktori, maka program akan melakukan pemanggilan fungsi categorizeFiles secara rekursif pada direktori tersebut. Proses rekursif ini akan terus dilakukan pada setiap direktori yang ditemukan hingga seluruh isi direktori terkategorikan.

```c
int countOtherFiles(const char *current_dir_path)
{
    int count = 0;
    int dirs = 1;
    
    strcpy(directory_path, current_dir_path);
```
Fungsi ini memiliki dua variabel lokal, yaitu count dan dirs, yang digunakan untuk menghitung jumlah file dan subdirektori. current_dir_path adalah path direktori yang akan dihitung. Fungsi strcpy digunakan untuk mengkopi current_dir_path ke dalam variabel directory_path.

```c
    while (true)
    {
        int found = 0;
        DIR *dir;
        struct dirent *ent;

        if ((dir = opendir(directory_path)) != NULL)
        {
            while ((ent = readdir(dir)) != NULL)
            {
                if (ent->d_type == DT_REG)
                {
                    count++;
                    found = 1;
                }
            }
            closedir(dir);
        }

        if (!found)
        {
            break;
        }

        dirs++;
        snprintf(directory_path, sizeof(directory_path), "%s (%d)", current_dir_path, dirs);
    }

    return count;
}
```
Pada bagian ini, dilakukan pengulangan untuk mencari file-file dalam direktori yang sedang diiterasi. Variabel found digunakan untuk menandakan apakah ada file yang ditemukan dalam iterasi saat ini. Jika dir berhasil dibuka dengan opendir, maka dilakukan pengulangan untuk membaca semua entri dalam direktori tersebut. Jika entri yang sedang dibaca adalah file (ent->d_type == DT_REG), maka jumlah count akan ditambah satu dan found akan diubah menjadi 1.
Setelah pengulangan selesai, direktori ditutup dengan closedir(dir). Jika tidak ada file yang ditemukan (!found), maka pengulangan dihentikan dengan menggunakan break.
Jika ada file yang ditemukan, jumlah dirs (jumlah subdirektori) akan ditambah satu, dan directory_path akan diubah menjadi current_dir_path diikuti dengan nomor subdirektori yang sedang diiterasi.
Terakhir, fungsi countOtherFiles akan mengembalikan jumlah file yang ditemukan (count).

```c
void getAmountOfFiles(){
    system("ls -R categorized > temp.txt");
```
Fungsi system() dipanggil untuk menjalankan perintah shell di dalam program C. Perintah ini akan menjalankan command ls -R categorized dalam shell, yang akan menampilkan daftar file di dalam folder "categorized" dan subdirektori-nya. Hasil output dari perintah tersebut kemudian akan disimpan di dalam file temporary "temp.txt".

```c
    FILE *temp_file = fopen("temp.txt", "r");
    char line[256];
```
Setelah itu, program membuka file "temp.txt" yang telah dibuat sebelumnya dan menyimpannya ke dalam variabel temp_file. Kemudian, program membuat sebuah array line berukuran 256 karakter.

```c
    int png_count = 0;
    int jpg_count = 0;
    int emc_count = 0;
    int xyz_count = 0;
    int js_count = 0;
    int py_count = 0;
    int txt_count = 0;
    int other_count = 0;
```
Variabel-variabel ini akan digunakan untuk menyimpan jumlah file yang ditemukan pada setiap jenis ekstensi file.

```c
 while (fgets(line, sizeof(line), temp_file)) {

    line[strcspn(line, "\n")] = 0;

    if (line[strlen(line) - 1] == ':') {
      continue;
    } else {
      
      char * file_name = strrchr(line, '/');
      if (file_name != NULL) {
        file_name++;
      } else {
        file_name = line;
      }

      
      int is_valid = 0;
      int exist = 0;
      for (int i = 0; i < ext_size; i++) {
        if (strstr(file_name, extensions[i]) != NULL) {
          exist++;
          is_valid = 1;
          break;
        }
      }

      if (is_valid) {
        if (strstr(file_name, ".png") != NULL) {
          png_count++;
        } else if (strstr(file_name, ".jpg") != NULL) {
          jpg_count++;
        } else if (strstr(file_name, ".emc") != NULL) {
          emc_count++;
        } else if (strstr(file_name, ".xyz") != NULL) {
          xyz_count++;
        } else if (strstr(file_name, ".js") != NULL) {
          js_count++;
        } else if (strstr(file_name, ".py") != NULL) {
          py_count++;
        } else if (strstr(file_name, ".txt") != NULL) {
          txt_count++;
        }
      }
    }
  }
```
Program akan membaca isi dari file "temp.txt" yang telah dibuat sebelumnya dan melakukan perhitungan jumlah file untuk setiap jenis ekstensi file. Pada baris pertama, program akan menghapus karakter newline pada akhir baris line[strcspn(line, "\n")] = 0;. Kemudian, program akan memeriksa apakah baris tersebut merupakan nama direktori. Jika ya, program akan melewati baris tersebut dan melanjutkan ke baris berikutnya dengan menggunakan perintah continue;. Jika bukan, program akan mengekstrak nama file dari path dengan menggunakan fungsi strrchr() dan memeriksa apakah ekstensi dari file tersebut valid. Untuk itu, program melakukan iterasi pada array extensions yang berisi jenis-jenis ekstensi file yang dianggap valid dan memeriksa apakah file tersebut memiliki ekstensi yang sama dengan yang terdapat pada array tersebut. Jika valid, program akan menambahkan jumlah file untuk setiap jenis ekstensi yang ditemukan.

```c
    printf("Jumlah file txt: %d\n", txt_count);
    printf("Jumlah file emc: %d\n", emc_count);
    printf("Jumlah file jpg: %d\n", jpg_count);
    printf("Jumlah file png: %d\n", png_count);
    printf("Jumlah file js: %d\n", js_count);
    printf("Jumlah file xyz: %d\n", xyz_count);
    printf("Jumlah file py: %d\n", py_count);
```
Baris ini berfungsi untuk menampilkan hasil perhitugan yang dilakukan pada bagian sebelumnya.

```c
    fclose(temp_file);

    remove("temp.txt");
}
```
Bagian ini menutup temp_file dan kemudian melakukan penghapusan dari file temp.txt.

```c
void createLog(const char *message)
{
    FILE *log_file = fopen("log.txt", "a");
```
Fungsi createLog digunakan untuk membuat file log dengan pesan tertentu. Baris kedua digunakan untuk membuka file log.txt dengan mode "a" (append) yang artinya data baru akan ditambahkan ke akhir file tanpa menghapus data yang sudah ada sebelumnya.

```c
    if (log_file != NULL)
    {
        time(&current_time);
        timeline = localtime(&current_time);

        strftime(buff, sizeof(buff), "%d-%m-%Y %H:%M:%S", timeline);
        fprintf(log_file, "%s %s\n", buff, message);
        fclose(log_file);
    }
```
Baris ini merupakan kondisi apabila file berhasil dibuka. Jika file berhasil dibuka, program akan mendapatkan waktu saat ini dan memformatnya menjadi string dengan menggunakan fungsi strftime. Kemudian, pesan yang diberikan akan dicetak di awal baris baru dan diakhiri dengan menutup file.

```c
int main()
{

    pthread_t tid;
```
Variabel tid bertipe pthread_t digunakan untuk menampung ID dari thread yang dibuat nanti.

```c
    mkdir("categorized", 0700);
    createLog("MADE categorized");

    mkdir("categorized/other", 0700);
    createLog("MADE categorized/other");
```
Baris ini digunakan untuk membuat direktori "categorized" dan "categorized/other" dengan menggunakan fungsi mkdir. Angka 0700 merupakan mode akses (permission) yang diberikan ke direktori tersebut. Selain itu, setiap kali direktori baru dibuat, fungsi createLog dipanggil untuk mencatatnya di file log.

```c
    pthread_create(&tid, NULL, categorizeFiles, files_dir);
    pthread_join(tid, NULL);
```
Baris ini digunakan untuk membuat thread baru dengan menggunakan fungsi pthread_create. Thread baru tersebut akan menjalankan fungsi categorizeFiles dengan parameter files_dir. Setelah selesai menjalankan thread, program akan menunggu hingga thread tersebut selesai dengan menggunakan fungsi pthread_join.

```c
    printf("Jumlah file : ");
    getAmountOfFiles();    
```
Program akan menampilkan pesan "Jumlah file : " dan kemudian memanggil fungsi getAmountOfFiles untuk menghitung jumlah file di direktori utama.

```c
    int others = countOtherFiles("categorized/other");
    printf("Jumlah isi subdirektori other: %d\n", others);
```
Program akan menghitung jumlah file di subdirektori "categorized/other" dengan memanggil fungsi countOtherFiles dan menampilkannya di layar.

```c
    return 0;
}
```
Fungsi main akan mengembalikan nilai 0 dan program berakhir.

Hasil Output:
https://ibb.co/BGNFBSf

logchecker.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
```
Baris ini adalah header yang menyatakan library atau pustaka standar dari bahasa C yang akan digunakan pada program ini.

```c
#define MAX_EXT_COUNT 100
#define MAX_EXT_LEN 10
```
Baris ini mendefinisikan konstanta MAX_EXT_COUNT dan MAX_EXT_LEN dengan nilai 100 dan 10 masing-masing. Konstanta ini digunakan untuk alokasi memori saat memproses ekstensi file.

```c
char ext_list[MAX_EXT_COUNT][MAX_EXT_LEN];
```
Baris ini deklarasi variabel ext_list sebagai array dua dimensi bertipe data karakter dengan ukuran MAX_EXT_COUNT x MAX_EXT_LEN. Variabel ini akan menyimpan list ekstensi file yang akan diproses.

```c
void printAccessCount() {
  FILE * log_file = fopen("log.txt", "r");
  char line[256];
  int access_count = 0;

  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
      if (strstr(line, "ACCESSED") != NULL) {
        access_count++;
      }
    }
    fclose(log_file);

    printf("Banyaknya ACCESSED: %d\n", access_count);
  } else {
    printf("Gagal membuka file log.txt\n");
  }
}
```
Fungsi printAccessCount() digunakan untuk mencetak banyaknya kali akses file yang ada pada file log log.txt. Pertama, variabel log_file dibuka dengan fungsi fopen() pada mode read-only ("r"). Kemudian, setiap baris file log dibaca satu persatu menggunakan fungsi fgets(). Jika pada baris tersebut terdapat kata "ACCESSED", maka nilai variabel access_count akan bertambah 1. Setelah selesai membaca seluruh baris file log, variabel log_file ditutup dengan fungsi fclose(). Jika gagal membuka file atau file tidak ditemukan, program akan mencetak pesan "Gagal membuka file log.txt".

```c
void printFolderList() {
  FILE * log_file = fopen("log.txt", "r");
  char line[256];
  char folder_name[256];
  int folder_counts[MAX_EXT_COUNT] = {
    0
  };
```
Fungsi printFolderList digunakan untuk mencetak daftar folder berdasarkan ekstensi file. Pertama-tama, program membuka file log.txt dengan mode "r" (read) dan membuat variabel karakter line dengan panjang 256 karakter, variabel karakter folder_name dengan panjang 256 karakter, dan variabel integer folder_counts sepanjang MAX_EXT_COUNT (yang diinisialisasi menjadi 0).

```c
  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
      if (strstr(line, "MADE") != NULL) {
        char * token = strtok(line, " ");
        while (token != NULL) {
          if (strstr(token, "categorized/") != NULL) {
            strcpy(folder_name, token);
            break;
          }
          token = strtok(NULL, " ");
        }

        for (int i = 0; i < MAX_EXT_COUNT; i++) {
          if (strcmp(ext_list[i], "") == 0) {
            break;
          }

          if (strstr(folder_name, ext_list[i]) != NULL) {
            folder_counts[i]++;
            break;
          }
        }
      }
    }
    fclose(log_file);

    printf("List folder:\n");
    for (int i = 0; i < MAX_EXT_COUNT; i++) {
      if (strcmp(ext_list[i], "") == 0) {
        break;
      }

      printf("%s : %d\n", ext_list[i], folder_counts[i]);
    }
  } else {
    printf("Gagal membuka file log.txt\n");
  }
}
```
Selanjutnya, program akan melakukan iterasi pada setiap baris dari file log.txt menggunakan perulangan while dan fgets. Jika baris tersebut mengandung string "MADE", maka program akan memproses baris tersebut untuk mencari nama folder yang terkait dengan file yang baru saja dibuat. Pertama-tama, program menggunakan strtok untuk membagi baris menjadi beberapa token berdasarkan spasi. Program kemudian melakukan iterasi pada semua token tersebut untuk mencari token yang mengandung string "categorized/". Jika ditemukan, program akan menyimpan nama folder ke dalam variabel folder_name.
Setelah itu, program akan melakukan perulangan pada setiap ekstensi file yang ada pada ext_list (yang telah dibaca dari file extensions.txt), dan jika folder_name mengandung ekstensi tersebut, maka program akan meningkatkan nilai folder_counts pada indeks yang sesuai.
Setelah selesai melakukan analisis pada semua baris file log.txt, program akan menutup file log.txt dan mencetak daftar folder beserta jumlah file pada setiap folder tersebut.

```c
void printFileCount() {
  FILE * log_file = fopen("log.txt", "r");
```
Fungsi printFileCount akan menampilkan jumlah file berdasarkan jenisnya. Kemudian fungsi membuka file log.txt dengan mode baca ("r") dan menyimpannya ke dalam variabel log_file.

```c
  char line[256];
  int png_count = 0;
  int jpg_count = 0;
  int emc_count = 0;
  int xyz_count = 0;
  int js_count = 0;
  int py_count = 0;
  int txt_count = 0;
  int other_count = 0;
```
Memperkenalkan variabel-variabel untuk menghitung jumlah file dari setiap jenisnya.

```c
  if (log_file != NULL) {
    while (fgets(line, sizeof(line), log_file)) {
```
Mengecek apakah file log.txt berhasil dibuka atau tidak. Lalu Menggunakan loop while untuk membaca file log baris per baris menggunakan fungsi fgets, kemudian menyimpannya ke dalam variabel line.

```c
      if (strstr(line, "ACCESSED") != NULL) {
        continue;
      }
```
Mengecek apakah baris pada file log mengandung kata "ACCESSED". Jika iya, maka proses loop lanjut tanpa menjalankan kode di bawahnya.

```c
      if (strstr(line, "MOVED") != NULL) {
```
Mengecek apakah baris pada file log mengandung kata "MOVED". Jika iya, maka kode di bawahnya akan dijalankan.

```c
        char * token = strtok(line, " ");
        char file_ext[MAX_EXT_LEN];

        while (token != NULL) {
          if (strstr(token, ".") != NULL) {
            strcpy(file_ext, token);
            break;
          }
          token = strtok(NULL, " ");
        }
```
Menggunakan fungsi strtok untuk memisahkan setiap kata pada variabel line, kemudian menyimpan hasil pemisahan pada variabel token. Selanjutnya, menggunakan loop while untuk mencari kata yang memiliki tanda titik (".") dan menyimpannya ke dalam variabel file_ext.

```c
        if (strstr(file_ext, "png") != NULL) {
          png_count++;
        } else if (strstr(file_ext, "jpg") != NULL) {
          jpg_count++;
        } else if (strstr(file_ext, "emc") != NULL) {
          emc_count++;
        } else if (strstr(file_ext, "xyz") != NULL) {
          xyz_count++;
        } else if (strstr(file_ext, "js") != NULL) {
          js_count++;
        } else if (strstr(file_ext, "py") != NULL) {
          py_count++;
        } else if (strstr(file_ext, "txt") != NULL) {
          txt_count++;
        } else {
          other_count++;
        }
```
Mengecek tipe file dari variabel file_ext yang telah ditemukan sebelumnya, dan menambahkan jumlah sesuai dengan jenis file tersebut. Jika tidak ada jenis file yang cocok, maka jumlah other_count akan ditambah.

```c
    }
    fclose(log_file);

    printf("Jumlah file txt: %d\n", txt_count);
    printf("Jumlah file emc: %d\n", emc_count);
    printf("Jumlah file jpg: %d\n", jpg_count);
    printf("Jumlah file png: %d\n", png_count);
    printf("Jumlah file js: %d\n", js_count);
    printf("Jumlah file xyz: %d\n", xyz_count);
    printf("Jumlah file py: %d\n", py_count);
    printf("Jumlah file other: %d\n", other_count);
  } else {
    printf("Gagal membuka file log.txt\n");
  }
```
Baris ini Menampilkan jumlah file dari setiap jenisnya menggunakan fungsi printf. Kemudian mencetak pesan kesalahan jika file log.txt gagal dibuka.

```c
int main() {
  char line[256];
  int ext_count = 0;
```
Baris ini memanggil fungsi main yang merupakan fungsi utama. Variabel char menyimpan string atau baris yang dibaca dari file. Variabel int ext_count menghitung jumlah ekstensi yang berhasil diproses dan disimpan pada array ext_list.

```c
  FILE * ext_file = fopen("extensions.txt", "r");
```
Membuka file extensions.txt dengan mode baca ("r") dan menyimpannya ke dalam variabel ext_file.

```c
  if (ext_file != NULL) {
    while (fgets(line, sizeof(line), ext_file)) {
      line[strcspn(line, "\r\n")] = 0; // menghilangkan newline atau carriage return
      strcpy(ext_list[ext_count], line);
      ext_count++;
    }
    fclose(ext_file);
  } else {
    printf("Gagal membuka file extensions.txt\n");
    return 1;
  }
```
Mengecek apakah file extensions.txt berhasil dibuka atau tidak. Jika iya, maka menggunakan loop while untuk membaca file baris per baris menggunakan fungsi fgets, dan menghilangkan karakter newline atau carriage return pada setiap baris menggunakan fungsi strcspn. Kemudian, memasukkan hasil pembacaan tersebut ke dalam variabel array ext_list. Variabel ext_count digunakan untuk menghitung banyaknya ekstensi yang telah dibaca. Setelah selesai membaca file, file extensions.txt akan ditutup menggunakan fungsi fclose. Jika file gagal dibuka, maka mencetak pesan kesalahan dan keluar dari program menggunakan return 1;.

```c
  printf("Pilih operasi:\n");
  printf("1. Banyaknya akses file\n");
  printf("2. List folder berdasarkan ekstensi file\n");
  printf("3. Jumlah file berdasarkan jenis\n");
```
Menampilkan menu pilihan operasi pada layar menggunakan fungsi printf, kemudian menerima input dari pengguna menggunakan fungsi scanf dan menyimpannya ke dalam variabel choice.

```c
  int choice;
  scanf("%d", & choice);

  switch (choice) {
  case 1:
    printAccessCount();
    break;
  case 2:
    printFolderList();
    break;
  case 3:
    printFileCount();
    break;
  default:
    printf("Operasi tidak valid\n");
    break;
  }
```
Menggunakan statement switch untuk memilih operasi berdasarkan input yang diterima sebelumnya. Jika choice sama dengan 1, maka akan memanggil fungsi printAccessCount. Jika choice sama dengan 2, maka akan memanggil fungsi printFolderList. Jika choice sama dengan 3, maka akan memanggil fungsi printFileCount. Jika choice tidak sama dengan ketiga angka tersebut, maka mencetak pesan kesalahan.

```c
  return 0;
}
```
Mengakhiri program dengan mengembalikan nilai 0.

Hasil Output:
https://ibb.co/1Q155sT